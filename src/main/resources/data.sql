insert into role(name) values ('ROLE_DIRECTOR'),('ROLE_FILIAL_MANAGER'),('ROLE_FILIAL_DIRECTOR'),('ROLE_NUMBER_MANAGER'),
                              ('ROLE_STAFF_MANAGER'),('ROLE_TARIFF_MANAGER'),('ROLE_STAFF'),('ROLE_CLIENT');

insert into usr( id, account_non_expired, account_non_locked, created_at, created_by, credentials_non_expired, enabled, first_name, last_name, password, updated_at, updated_by, username, filial_id, role_id)
values ('a215434a-01bc-11ec-9a03-0242ac130003',
        true,
        true,
        now(),
        null,
        true,
        true,
        'ism',
        'familiya',
        '$2a$10$eoMcclSxxvWldc215xnC8e0jIDqAa17Rd/15w/MRn2yo0/P3c1YrG', --parol=123
        null,
        null,
        'root',
        null,
        1 );
