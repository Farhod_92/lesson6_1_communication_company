package uzb.farhod.lesson6_1_communication_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson6_1_communication_company.entity.Payment;
import uzb.farhod.lesson6_1_communication_company.entity.Simcard;

import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {
}
