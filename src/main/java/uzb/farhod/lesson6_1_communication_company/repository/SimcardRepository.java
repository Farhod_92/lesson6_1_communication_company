package uzb.farhod.lesson6_1_communication_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uzb.farhod.lesson6_1_communication_company.entity.Simcard;

import java.util.List;
import java.util.UUID;

public interface SimcardRepository extends JpaRepository<Simcard, UUID> {
    //OYLIK**************
    @Query(value = "select sum(t.price) from usr u join simcard s on u.id = s.user_id join tariff t on s.tariff_id = t.id\n" +
            "where u.created_by in (select id from usr where usr.role_id=4) and\n" +
            "extract(year from u.created_at)= :year and extract(month from u.created_at )= :month",nativeQuery = true)
    double monthlySoldSims(@Param("year") Integer year, @Param("month") Integer month);


    @Query(value = "select sum(p.price) from simcard s join simcard_packets sp on s.id = sp.simcard_id \n" +
            "join packet p on p.id = sp.packets_id\n" +
            "where extract(year from s.packet_expire_date)= :year and \n" +
            "( extract(month from s.packet_expire_date-p.expire_after_days)= :month )", nativeQuery = true)
    double monthlySoldPackets(@Param("year") Integer year, @Param("month") Integer month);

  @Query(value = "select p.name as name ,count(sp.packets_id) as count from  simcard s join simcard_packets sp on s.id = sp.simcard_id join packet p on p.id = sp.packets_id\n" +
          "where extract(year from s.packet_expire_date)= :year and\n" +
          "(extract(month from  s.packet_expire_date-p.expire_after_days)= :month ) " +
          "group by sp.packets_id,p.name order by sp.packets_id desc limit 3", nativeQuery = true)
    List<Object[]> mostSoldPackets(@Param("year") Integer year, @Param("month") Integer month);

    //CHORAKLIK***************
    @Query(value = "select sum(t.price) from usr u join simcard s on u.id = s.user_id join tariff t on s.tariff_id = t.id " +
            "where u.created_by in (select id from usr where usr.role_id=4) and " +
            "extract(year from u.created_at)= :year and extract(quarter from u.created_at )= :quarter",nativeQuery = true)
    double quarterlySoldSims(@Param("year") Integer year, @Param("quarter") Integer quarter);

    @Query(value = "select sum(p.price) from simcard s join simcard_packets sp on s.id = sp.simcard_id \n" +
            "join packet p on p.id = sp.packets_id\n" +
            "where extract(year from s.packet_expire_date)= :year and \n" +
            "( extract(quarter from s.packet_expire_date-p.expire_after_days)= :quarter )", nativeQuery = true)
    double quarterlySoldPackets(@Param("year") Integer year, @Param("quarter") Integer quarter);

    @Query(value = "select p.name as name ,count(sp.packets_id) as count from  simcard s join simcard_packets sp on s.id = sp.simcard_id join packet p on p.id = sp.packets_id\n" +
          "where extract(year from s.packet_expire_date)= :year and\n" +
          "(extract(quarter from  s.packet_expire_date-p.expire_after_days)= :quarter ) " +
          "group by sp.packets_id,p.name order by sp.packets_id desc limit 3", nativeQuery = true)
    List<Object[]> mostSoldPacketsOnQuarter(@Param("year") Integer year, @Param("quarter") Integer quarter);

    //KUNLIK***************
    @Query(value = "select sum(t.price) from usr u join simcard s on u.id = s.user_id join tariff t on s.tariff_id = t.id " +
            "where u.created_by in (select id from usr where usr.role_id=4) and " +
            "extract(year from u.created_at)= :year and extract(month from u.created_at )= :month and " +
            "extract(day from u.created_at )= :day ",nativeQuery = true)
    Double dailySoldSims(@Param("year") Integer year, @Param("month") Integer month, @Param("day") Integer day);

    @Query(value = "select sum(p.price) from simcard s join simcard_packets sp on s.id = sp.simcard_id \n" +
            "join packet p on p.id = sp.packets_id\n" +
            "where extract(year from s.packet_expire_date)= :year and \n" +
            "(extract(month from s.packet_expire_date-p.expire_after_days)= :month ) and" +
            " (extract(day from s.packet_expire_date-p.expire_after_days)= :day )", nativeQuery = true)
    Double dailylySoldPackets(@Param("year") Integer year, @Param("month") Integer month, @Param("day") Integer day);

    @Query(value = "select p.name as name ,count(sp.packets_id) as count from  simcard s join simcard_packets sp on s.id = sp.simcard_id join packet p on p.id = sp.packets_id\n" +
          "where extract(year from s.packet_expire_date)= :year and\n" +
          "(extract(month from  s.packet_expire_date-p.expire_after_days)= :month ) and " +
          "(extract(day from  s.packet_expire_date-p.expire_after_days)= :day ) " +
          "group by sp.packets_id,p.name order by sp.packets_id desc limit 3", nativeQuery = true)
    List<Object[]> mostSoldPacketsOnDay(@Param("year") Integer year, @Param("month") Integer month, @Param("day") Integer day);



}
