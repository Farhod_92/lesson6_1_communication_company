package uzb.farhod.lesson6_1_communication_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson6_1_communication_company.entity.EntertaimentService;
import uzb.farhod.lesson6_1_communication_company.entity.Tariff;

@RepositoryRestResource(path = "entertaimentService")
public interface EntertaimentServiceRepository extends JpaRepository<EntertaimentService, Integer> {
}
