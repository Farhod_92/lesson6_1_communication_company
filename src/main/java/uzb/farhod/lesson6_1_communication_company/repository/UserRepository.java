package uzb.farhod.lesson6_1_communication_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson6_1_communication_company.entity.Filial;
import uzb.farhod.lesson6_1_communication_company.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    boolean existsByUsername(String username);
    Optional<User> findByUsername(String username);
    List<User> findAllByFilial(Filial filial);

}
