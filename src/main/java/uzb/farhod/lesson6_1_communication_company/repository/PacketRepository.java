package uzb.farhod.lesson6_1_communication_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson6_1_communication_company.entity.Packet;
import uzb.farhod.lesson6_1_communication_company.entity.Turniket;

import java.util.UUID;
@RepositoryRestResource(path = "packet")
public interface PacketRepository extends JpaRepository<Packet, Integer> {
}
