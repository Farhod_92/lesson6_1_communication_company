package uzb.farhod.lesson6_1_communication_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson6_1_communication_company.entity.Filial;

public interface FilialRepository extends JpaRepository<Filial, Integer> {
    boolean existsByName(String name);
}
