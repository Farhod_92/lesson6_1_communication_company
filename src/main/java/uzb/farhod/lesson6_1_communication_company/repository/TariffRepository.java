package uzb.farhod.lesson6_1_communication_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson6_1_communication_company.entity.Tariff;

import java.util.List;

@RepositoryRestResource(path = "tariff")
public interface TariffRepository extends JpaRepository<Tariff, Integer> {
    //OYLIK******************
    @Query(value = "select t.name as nomi , count(s.tariff_id) as soni from simcard s join tariff t on t.id = s.tariff_id " +
            "where s.active=true group by s.tariff_id, t.name order by count(*) desc limit 3", nativeQuery = true)
    List<Object[]> mostActiveTariffs();

    @Query(value = "select t.name as nomi, count(s.tariff_id) as soni from simcard s join tariff t on t.id = s.tariff_id " +
            "where s.active=false group by s.tariff_id, t.name order by count(*) desc limit 3", nativeQuery = true)
    List<Object[]> mostPAssiveTariffs();

}
