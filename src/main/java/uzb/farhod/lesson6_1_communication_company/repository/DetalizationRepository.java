package uzb.farhod.lesson6_1_communication_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uzb.farhod.lesson6_1_communication_company.entity.Detalization;
import uzb.farhod.lesson6_1_communication_company.entity.Simcard;
import uzb.farhod.lesson6_1_communication_company.entity.User;

import java.util.List;
import java.util.UUID;

public interface DetalizationRepository extends JpaRepository<Detalization, UUID> {
    @Query(value = "select d.id, d.by, d.time, d.name, d.amount, d.measurement_id, d.user_action from detalization d join usr u on u.id=d.by \n" +
            "where extract(year from d.time)= :year and extract(month from d.time)= :month \n" +
            "and u.id= :userId", nativeQuery = true)
    List<Detalization> getDetalizations(@Param("year") Integer year,@Param("month") Integer month, @Param("userId") UUID userId);
}
