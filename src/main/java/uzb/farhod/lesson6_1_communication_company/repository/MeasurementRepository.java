package uzb.farhod.lesson6_1_communication_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson6_1_communication_company.entity.Measurement;

@RepositoryRestResource(path = "measurement")
public interface MeasurementRepository extends JpaRepository<Measurement, Integer> {
    Measurement findByName(String name);
}
