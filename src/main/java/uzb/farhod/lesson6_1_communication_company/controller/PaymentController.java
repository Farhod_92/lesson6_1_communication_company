package uzb.farhod.lesson6_1_communication_company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.payload.PaymentDto;
import uzb.farhod.lesson6_1_communication_company.service.PaymentService;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {
    @Autowired
    PaymentService paymentService;

    @PostMapping
    public ResponseEntity<?> payment(@RequestBody PaymentDto paymentDto){
        ApiResponse apiResponse = paymentService.payment(paymentDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
}
