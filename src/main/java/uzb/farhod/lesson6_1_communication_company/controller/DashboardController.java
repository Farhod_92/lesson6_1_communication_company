package uzb.farhod.lesson6_1_communication_company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.service.DashboardService;

@PreAuthorize(value = "hasRole('ROLE_DIRECTOR')")
@RestController
@RequestMapping("/api/dashboard")
public class DashboardController {

    @Autowired
    DashboardService dashboardService;

    @GetMapping("/quarterly/{quarter}")
    public ResponseEntity<?> getQuarterlyReport(@PathVariable Integer quarter){
        ApiResponse quarterlyReport = dashboardService.getQuarterlyReport(quarter);
        return ResponseEntity.ok(quarterlyReport);
    }

    @GetMapping("/monthly")
    public ResponseEntity<?> getMonthlyReport(){
        ApiResponse monthlyReport = dashboardService.getMonthlyReport();
        return ResponseEntity.ok(monthlyReport);
    }

    @GetMapping("/daily")
    public ResponseEntity<?> getDailyReport(){
        ApiResponse dailyReport = dashboardService.getDailyReport();
        return ResponseEntity.ok(dailyReport);
    }


}
