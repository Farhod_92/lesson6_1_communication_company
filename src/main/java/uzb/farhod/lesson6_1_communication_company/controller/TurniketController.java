package uzb.farhod.lesson6_1_communication_company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_1_communication_company.entity.Turniket;
import uzb.farhod.lesson6_1_communication_company.entity.User;
import uzb.farhod.lesson6_1_communication_company.repository.TurniketRepository;
import uzb.farhod.lesson6_1_communication_company.service.AuthService;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("api/turniket")
@PreAuthorize(value = "hasAnyRole('ROLE_DIRECTOR','ROLE_FILIAL_MANAGER','ROLE_FILIAL_DIRECTOR','ROLE_NUMBER_MANAGER','ROLE_STAFF_MANAGER','ROLE_STAFF')")
public class TurniketController {
    @Autowired
    private AuthService authService;

    @Autowired
    private TurniketRepository turniketRepository;

    @PostMapping("/{come}")
    public ResponseEntity<?> add(@Valid @PathVariable boolean come){
        User user= authService.getLoggedUser();
        Turniket turniket=new Turniket();
        turniket.setUser(user);
        turniket.setCome(come);
        turniketRepository.save(turniket);
        return ResponseEntity.status(201).body("saved");
    }
}
