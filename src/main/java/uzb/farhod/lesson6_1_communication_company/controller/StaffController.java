package uzb.farhod.lesson6_1_communication_company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_1_communication_company.entity.User;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.payload.StaffEditDto;
import uzb.farhod.lesson6_1_communication_company.payload.StaffRegisterDto;
import uzb.farhod.lesson6_1_communication_company.service.AuthService;
import uzb.farhod.lesson6_1_communication_company.service.StaffService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api/staff")
@PreAuthorize(value = "hasAnyRole('ROLE_STAFF_MANAGER','ROLE_FILIAL_DIRECTOR','ROLE_STAFF')")
public class StaffController {

    @Autowired
    AuthService authService;

    @Autowired
    StaffService staffService;

    @PreAuthorize(value = "hasAnyRole('ROLE_STAFF_MANAGER','ROLE_FILIAL_DIRECTOR')")
    @PostMapping("/register")
    public HttpEntity<?> register(@Valid @RequestBody StaffRegisterDto staffRegisterDto){
        ApiResponse apiResponse = authService.registerStaff(staffRegisterDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasAnyRole('ROLE_STAFF_MANAGER','ROLE_FILIAL_DIRECTOR')")
    @GetMapping
    public HttpEntity<?> getAll(){
        List<User> userList = staffService.getAll();
        return ResponseEntity.ok(userList);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOne(@PathVariable UUID id){
       User user = staffService.getOne(id);
        return ResponseEntity.ok(user);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> edit(@PathVariable UUID id, @Valid @RequestBody StaffEditDto staffEditDto){
       ApiResponse apiResponse = staffService.edit(id,staffEditDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasAnyRole('ROLE_STAFF_MANAGER','ROLE_FILIAL_DIRECTOR')")
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable UUID id){
        ApiResponse apiResponse = staffService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()?204:409).body(apiResponse);
    }

    //validatsiya message
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
