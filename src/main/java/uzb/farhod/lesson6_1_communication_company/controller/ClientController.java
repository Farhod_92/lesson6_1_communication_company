package uzb.farhod.lesson6_1_communication_company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.payload.ClientRegisterDto;
import uzb.farhod.lesson6_1_communication_company.service.ClientService;
import uzb.farhod.lesson6_1_communication_company.service.ClientActionService;
import uzb.farhod.lesson6_1_communication_company.service.UssdService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/client")
@PreAuthorize(value = "hasAnyRole('ROLE_NUMBER_MANAGER','ROLE_CLIENT')")
public class ClientController {

    @Autowired
    ClientService clientService;

    @Autowired
    UssdService ussdService;

    @Autowired
    ClientActionService clientActionService;


    @PreAuthorize(value = "hasRole('ROLE_NUMBER_MANAGER')")
    @PostMapping("/register")
    public HttpEntity<?> register(@Valid @RequestBody ClientRegisterDto clientRegisterDto){
        ApiResponse apiResponse = clientService.register(clientRegisterDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }


    @PostMapping("ussd/{ussd}")
    public HttpEntity<?> executeUssd(@PathVariable String ussd){
        ApiResponse apiResponse = ussdService.executeUssd(ussd);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @GetMapping("/details")
    public ResponseEntity<?> getDetalization(@RequestParam Integer year, @RequestParam Integer month){
        return clientService.getDetalization(year, month);
    }


    @PreAuthorize(value = "hasRole('ROLE_CLIENT')")
    @GetMapping("/call/{callType}/{minutes}")
    //callType in yoki out bo'lishi mumkin
    public ResponseEntity<?> makeCAll(@PathVariable String callType, @PathVariable Integer minutes){
        ApiResponse apiResponse=clientActionService.makeCAll(callType, minutes);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasRole('ROLE_CLIENT')")
    @GetMapping("/sms")
    public ResponseEntity<?> sms(){
        ApiResponse apiResponse=clientActionService.sms();
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasRole('ROLE_CLIENT')")
    @GetMapping("/internet/{mb}")
    public ResponseEntity<?> internet(@PathVariable Integer mb){
        ApiResponse apiResponse=clientActionService.internet(mb);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


    //validatsiya message
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
