package uzb.farhod.lesson6_1_communication_company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_1_communication_company.entity.Filial;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.payload.FilialDto;
import uzb.farhod.lesson6_1_communication_company.service.FilialService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@PreAuthorize(value = "hasAnyRole('ROLE_DIRECTOR','ROLE_FILIAL_MANAGER')")
@RequestMapping("/api/filial")
public class FilialController {

    @Autowired
    FilialService filialService;

    @PostMapping
    public ResponseEntity<?> add(@RequestParam String name){
        ApiResponse apiResponse = filialService.add(name);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id){
        Filial one = filialService.getOne(id);
        return ResponseEntity.status(one==null?409:200).body(one);
    }

    @GetMapping
    public ResponseEntity<?> getAll(){
        List<Filial> filials = filialService.getAll();
        return ResponseEntity.status(filials.isEmpty()?409:200).body(filials);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @Valid @RequestBody FilialDto filialDto){
        ApiResponse api = filialService.edit(id, filialDto);
        return ResponseEntity.status(api.isSuccess()?200:409).body(api);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id){
        ApiResponse apiResponse = filialService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()?204:409).body(apiResponse);
    }

    @PostMapping("/setDirector")
    public ResponseEntity<?> setrDirector(@RequestParam Integer filialId, @RequestParam UUID userId){
        ApiResponse apiResponse = filialService.setDirector(filialId, userId);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    //validatsiya message
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
