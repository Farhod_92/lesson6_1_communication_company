package uzb.farhod.lesson6_1_communication_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson6_1_communication_company.entity.Filial;
import uzb.farhod.lesson6_1_communication_company.entity.User;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.payload.FilialDto;
import uzb.farhod.lesson6_1_communication_company.repository.FilialRepository;
import uzb.farhod.lesson6_1_communication_company.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class FilialService {
    @Autowired
    FilialRepository filialRepository;

    @Autowired
    UserRepository userRepository;

    public ApiResponse add(String name){
        boolean existsByName = filialRepository.existsByName(name);
        if(existsByName)
            return new ApiResponse("bunaqa filial bor", false);

        Filial filial=new Filial(name);
        filialRepository.save(filial);
        return new ApiResponse("filial qo'shildi", true);
    }

    public Filial getOne(Integer id){
        Optional<Filial> optionalFilial = filialRepository.findById(id);
        return optionalFilial.orElse(null);
    }

    public List<Filial> getAll(){
        return filialRepository.findAll();
    }

    public ApiResponse edit(Integer id, FilialDto filialDto){
        Optional<Filial> optionalFilial = filialRepository.findById(id);
        if(!optionalFilial.isPresent())
            return new ApiResponse("filial topilmadi", false);

        Optional<User> optionalUser = userRepository.findById(filialDto.getDirectorId());
        if(!optionalUser.isPresent())
            return new ApiResponse("user topilmadi", false);

        Filial filial= optionalFilial.get();
        filial.setName(filialDto.getName());
        filial.setDirector(optionalUser.get());
        filialRepository.save(filial);
        return new ApiResponse("filial edited", true);
    }

    public ApiResponse delete(Integer id){
        try {
            filialRepository.deleteById(id);
            return new ApiResponse("filial deleted", true);
        }catch (Exception e){
            return new ApiResponse(e.getMessage(), false);
        }
    }

    public ApiResponse setDirector(Integer filialId, UUID userId){
        Optional<Filial> optionalFilial = filialRepository.findById(filialId);
        if(!optionalFilial.isPresent())
            return new ApiResponse("filial topilmadi", false);

        Optional<User> optionalUser = userRepository.findById(userId);
        if(!optionalUser.isPresent())
            return new ApiResponse("user topilmadi", false);

        Filial filial = optionalFilial.get();
        filial.setDirector(optionalUser.get());
        filialRepository.save(filial);
        return new ApiResponse("filial directori belgilandi", true);
    }
}
