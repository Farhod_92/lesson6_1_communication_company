package uzb.farhod.lesson6_1_communication_company.service;


import com.google.gson.Gson;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfOutputStream;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.draw.SolidLine;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.LineSeparator;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uzb.farhod.lesson6_1_communication_company.entity.*;
import uzb.farhod.lesson6_1_communication_company.entity.enums.UserAction;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.payload.ClientRegisterDto;
import uzb.farhod.lesson6_1_communication_company.repository.*;

import java.io.*;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class ClientService {
    @Autowired
    AuthService authService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TariffRepository tariffRepository;

    @Autowired
    SimcardRepository simcardRepository;

    @Autowired
    DetalizationRepository detalizationRepository;

    @Autowired
    PacketRepository packetRepository;

    public ApiResponse register(ClientRegisterDto clientRegisterDto){
        ApiResponse authResponse = authService.registerClient(clientRegisterDto);
        if(!authResponse.isSuccess())
            return authResponse;

        Tariff tariff = tariffRepository.getById(clientRegisterDto.getTarifId());
        //tarif uchun oyning oxirigacha to'lanishi kerak bo'lgan summa
        double tolov = getTariffPaymentAmount(tariff);

        if(clientRegisterDto.getPayment()>tolov){
            Simcard simcard=new Simcard();
            simcard.setUser((User) authResponse.getObject());
            simcard.setBalance(clientRegisterDto.getPayment()-tolov);

            simcard.setTariffMegabyte(tariff.getMegabyte());
            simcard.setTariffInNetworkMinute(tariff.getInNetworkMinute());
            simcard.setTariffOutNetworkMinute(tariff.getOutNetworkMinute());
            simcard.setTariffSms(tariff.getSms());
            simcard.setTariff(tariff);
            simcard.setActive(true);
            simcard.setTariffActivationDate(LocalDate.now());
            simcardRepository.save(simcard);
            return new ApiResponse("user qo'shildi", true);
        }else{
            return new ApiResponse("bu tarifga ulanish uchun mablag' yetarli emas", false);
        }
    }

    //client o'zi tarifni almashtirsa
    @Transactional
    public ApiResponse changeTariff(Integer tariffId){
        Tariff tariff = tariffRepository.getById(tariffId);
        User user = authService.getLoggedUser();
        Simcard simcard = user.getSimcard();
        double tolov = getTariffPaymentAmount(tariff)+tariff.getPriceToSwitch();

        if(simcard.getBalance()<tolov)
            return new ApiResponse("tarifga o'tish uchun hisobda mablag' yetmaydi", false);

        simcard.setTariff(tariff);
        simcard.setBalance(simcard.getBalance()-tolov);
        simcard.setTariffMegabyte(tariff.getMegabyte());
        simcard.setTariffInNetworkMinute(tariff.getInNetworkMinute());
        simcard.setTariffOutNetworkMinute(tariff.getOutNetworkMinute());
        simcard.setTariffSms(tariff.getSms());
        simcard.setTariffActivationDate(LocalDate.now());
        simcardRepository.save(simcard);

        Detalization detalization =new Detalization();
        detalization.setUserAction(UserAction.CHANGE_TARIFF);
        detalization.setName(tariff.getName());
        detalizationRepository.save(detalization);
        return new ApiResponse("tarif almashtirildi", true);
    }

    @Transactional
    public ApiResponse buyPacket(Integer packetId){
        Packet newPacket = packetRepository.getById(packetId);
        User user = authService.getLoggedUser();
        Simcard simcard = user.getSimcard();
        Tariff simcardTariff = simcard.getTariff();

        if(!newPacket.getTariffs().contains(simcardTariff))
            return new ApiResponse("paketni bu tarifga ulab bo'lmaydi", false);

        if(simcard.getBalance()<newPacket.getPrice())
            return new ApiResponse("hisobingizda mablag' yetarli emas", false);

        List<Packet> simcardPackets = simcard.getPackets();
        if(newPacket.isRemainder()){
            if(newPacket.getPacketType().equals(UserAction.TRAFFIC_USE)){
                simcard.setPacketMegabyte(simcard.getPacketMegabyte()+ newPacket.getAmount());
            }else if(newPacket.getPacketType().equals(UserAction.IN_NETWORK_CALL)){
                simcard.setPacketInNetworkMinute(simcard.getPacketInNetworkMinute()+ newPacket.getAmount());
            }else if(newPacket.getPacketType().equals(UserAction.OUT_NETWORK_CALL)){
                simcard.setPacketOutNetworkMinute(simcard.getPacketOutNetworkMinute()+ newPacket.getAmount());
            }else if(newPacket.getPacketType().equals(UserAction.SMS_SEND)){
                simcard.setPacketSms(simcard.getPacketSms()+ newPacket.getAmount());
            }
        }else{
            if(newPacket.getPacketType().equals(UserAction.TRAFFIC_USE)){
                simcard.setPacketMegabyte( newPacket.getAmount());
            }else if(newPacket.getPacketType().equals(UserAction.IN_NETWORK_CALL)){
                simcard.setPacketInNetworkMinute(newPacket.getAmount());
            }else if(newPacket.getPacketType().equals(UserAction.OUT_NETWORK_CALL)){
                simcard.setPacketOutNetworkMinute( newPacket.getAmount());
            }else if(newPacket.getPacketType().equals(UserAction.SMS_SEND)){
                simcard.setPacketSms( newPacket.getAmount());
            }
        }

        simcardPackets.add(newPacket);
        simcard.setBalance(simcard.getBalance()-newPacket.getPrice());
        simcard.setPacketExpireDate(LocalDate.now().plusDays(newPacket.getExpireAfterDays()));
        simcardRepository.save(simcard);

        Detalization detalization =new Detalization();
        detalization.setUserAction(UserAction.BUY_PACKET);
        detalization.setName(newPacket.getName());
        detalization.setAmount(1);
        detalizationRepository.save(detalization);
        return new ApiResponse("yangi paket qo'shildi", true);
    }

    //tarif uchun oyning oxirigacha to'lanishi kerak bo'lgan summani hisoblash
    public double getTariffPaymentAmount(Tariff tariff){
        LocalDate now = LocalDate.now();
        int dayOfMonth = now.getDayOfMonth();
        int lengthOfMonth = now.lengthOfMonth();
        double birKunlikTolov = tariff.getPrice() / lengthOfMonth;
        return (lengthOfMonth - dayOfMonth) * birKunlikTolov;
    }

    public ApiResponse seeTariff() {
        try{
            User user = authService.getLoggedUser();
            String tariffName = user.getSimcard().getTariff().getName();
            return new ApiResponse("siz "+ tariffName+" tarifdasiz",true);
        }catch (Exception e){
            return new ApiResponse("tarif aniqlanmadi", false);
        }
    }

    public ResponseEntity<?> getDetalization(Integer year, Integer month){
        User loggedUser = authService.getLoggedUser();
        List<Detalization> detalizations =
                detalizationRepository.getDetalizations(year, month, loggedUser.getId());

        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        //"src/main/resources/pdfDocument.pdf"
       try( PdfWriter writer=new PdfWriter("src/main/resources/pdfDocument.pdf")){
           PdfDocument pdfDocument=new PdfDocument(writer);
           pdfDocument.addNewPage();
           Document document=new Document(pdfDocument);

           Paragraph paragraph = new Paragraph(loggedUser.getUsername() +" "+year+" - yil "+month + " -oy uchun detalizatsiya");
           document.add(paragraph);

           SolidLine solidLine=new SolidLine(1f);
           LineSeparator lineSeparator=new LineSeparator(solidLine);
           lineSeparator.setMargin(5);
           document.add(lineSeparator);

           float[] columns = {30f, 30, 300, 300, 300f};
           Table table = new Table(columns);
           table.addCell("№");
           table.addCell("Action");
           table.addCell("Name");
           table.addCell("Amount");
           table.addCell("Time");

           for (int i = 0; i < detalizations.size(); i++) {
               table.addCell(String.valueOf(i+1));

               table.addCell(detalizations.get(i).getUserAction().name());

               if(detalizations.get(i).getName()!=null)
                   table.addCell(detalizations.get(i).getName());
               else
                   table.addCell("");

               if(detalizations.get(i).getMeasurement()!=null)
               table.addCell(String.valueOf(detalizations.get(i).getAmount())
                       .concat( String.valueOf(detalizations.get(i).getMeasurement().getName())));
               else
                   table.addCell("");


               table.addCell(String.valueOf(detalizations.get(i).getTime()));
           }

           document.add(table);
           document.close();

           return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
               .header(HttpHeaders.CONTENT_DISPOSITION, "inline; fileName=details.pdf")
               .body(baos.toByteArray());

       } catch (FileNotFoundException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
       return null;
    }

}
