package uzb.farhod.lesson6_1_communication_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.repository.SimcardRepository;
import uzb.farhod.lesson6_1_communication_company.repository.TariffRepository;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class DashboardService {
    @Autowired
    SimcardRepository simcardRepository;
    
    @Autowired
    TariffRepository tariffRepository;
    //oylik hisobotlar
    public ApiResponse getMonthlyReport(){
        int year = LocalDate.now().getYear();
        int month = LocalDate.now().getMonthValue();

        //yangi ulanishlardan tushgan foyda
        double incomeFromNewConnections = simcardRepository.monthlySoldSims(year, month);
        //paketlar sotishdan foyda(paketlarni amal qilish muddati 1 oydan oshmaydi!)
        double incomeFromMonthlySoldPackets = simcardRepository.monthlySoldPackets(year, month);
        //Qaysi tarifflarda foydalanuvchilar eng aktiv top 3 talik
        List<Object[]> mostActiveTariffs = tariffRepository.mostActiveTariffs();
        //Qaysi tarifflarda foydalanuvchilar eng passiv eng yomon 3 talik
        List<Object[]> mostPAssiveTariffs = tariffRepository.mostPAssiveTariffs();
        //eng ko'p sotilgan paketlar
        List<Object[]> mostSoldPackets = simcardRepository.mostSoldPackets(year, month);

//        for (Object[] packet : mostSoldPackets) {
//            System.out.print("name= "+ packet[0]);
//            System.out.println(" count= "+ packet[1]);
//        }

        Map<String, Object> monthlyReport=new HashMap<>();
        monthlyReport.put("incomeFromNewConnections",incomeFromNewConnections);
        monthlyReport.put("incomeFromMonthlySoldPackets",incomeFromMonthlySoldPackets);
        monthlyReport.put("mostActiveTariffs",mostActiveTariffs);
        monthlyReport.put("mostPAssiveTariffs",mostPAssiveTariffs);
        monthlyReport.put("mostSoldPackets",mostSoldPackets);
        return new ApiResponse("Oylik hisobot", true,monthlyReport);
    }

    //choraklik hisobotlar
    public ApiResponse getQuarterlyReport(Integer quarter) {
        int year = LocalDate.now().getYear();

        //yangi ulanishlardan tushgan foyda
        double incomeFromNewConnections = simcardRepository.quarterlySoldSims(year, quarter);
        //paketlar sotishdan foyda(paketlarni amal qilish muddati 1 oydan oshmaydi!)
        double incomeFromMonthlySoldPackets = simcardRepository.quarterlySoldPackets(year, quarter);
        //eng ko'p sotilgan paketlar
        List<Object[]> mostSoldPackets = simcardRepository.mostSoldPacketsOnQuarter(year, quarter);


        Map<String, Object> quarterlyReport=new HashMap<>();
        quarterlyReport.put("incomeFromNewConnections",incomeFromNewConnections);
        quarterlyReport.put("incomeFromMonthlySoldPackets",incomeFromMonthlySoldPackets);
        quarterlyReport.put("mostSoldPackets",mostSoldPackets);
        return new ApiResponse("Choraklik hisobot", true,quarterlyReport);
    }

    //kunlik hisobotlar
    public ApiResponse getDailyReport() {
        int year = LocalDate.now().getYear();
        int month = LocalDate.now().getMonthValue();
        int day = LocalDate.now().getDayOfMonth();

        //yangi ulanishlardan tushgan foyda
        Double incomeFromNewConnections = simcardRepository.dailySoldSims(year, month, day);
        //paketlar sotishdan foyda(paketlarni amal qilish muddati 1 oydan oshmaydi!)
        Double incomeFromMonthlySoldPackets = simcardRepository.dailylySoldPackets(year, month, day);
        //eng ko'p sotilgan paketlar
        List<Object[]> mostSoldPackets = simcardRepository.mostSoldPacketsOnDay(year, month, day);

        Map<String, Object> dailyReport=new HashMap<>();
        dailyReport.put("incomeFromNewConnections",incomeFromNewConnections);
        dailyReport.put("incomeFromMonthlySoldPackets",incomeFromMonthlySoldPackets);
        dailyReport.put("mostSoldPackets",mostSoldPackets);
        return new ApiResponse("Kunlik hisobot", true,dailyReport);
    }
}
