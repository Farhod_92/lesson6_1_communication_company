package uzb.farhod.lesson6_1_communication_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uzb.farhod.lesson6_1_communication_company.entity.Payment;
import uzb.farhod.lesson6_1_communication_company.entity.Simcard;
import uzb.farhod.lesson6_1_communication_company.entity.User;
import uzb.farhod.lesson6_1_communication_company.entity.enums.PaymentType;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.payload.PaymentDto;
import uzb.farhod.lesson6_1_communication_company.repository.PaymentRepository;
import uzb.farhod.lesson6_1_communication_company.repository.SimcardRepository;
import uzb.farhod.lesson6_1_communication_company.repository.UserRepository;

import java.util.Optional;

@Service
public class PaymentService {
    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthService authService;

    @Autowired
    SimcardRepository simcardRepository;

    @Transactional
    public ApiResponse payment(PaymentDto paymentDto){
        Optional<User> optionalUser = userRepository.findByUsername(paymentDto.getToNumber());
        if(!optionalUser.isPresent())
            return new ApiResponse("abonent topilmadi", false);

        try {
            User user = optionalUser.get();
            PaymentType paymentType = PaymentType.valueOf(paymentDto.getPaymentType());
            Simcard simcard = user.getSimcard();
            simcard.setBalance(simcard.getBalance() + paymentDto.getAmount());
            simcardRepository.save(simcard);

            Payment payment = new Payment();
            payment.setAmount(paymentDto.getAmount());
            payment.setToNumber(user);
            payment.setPaymentType(paymentType);
            paymentRepository.save(payment);
            return new ApiResponse("to'landi", true);
        }catch (Exception e){
            return new ApiResponse("to'lov amalga oshmadi", false);
        }
    }
}
