package uzb.farhod.lesson6_1_communication_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uzb.farhod.lesson6_1_communication_company.entity.*;
import uzb.farhod.lesson6_1_communication_company.entity.enums.UserAction;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.repository.DetalizationRepository;
import uzb.farhod.lesson6_1_communication_company.repository.MeasurementRepository;
import uzb.farhod.lesson6_1_communication_company.repository.SimcardRepository;

import java.time.LocalDate;
import java.util.List;

@Service
public class ClientActionService {
    @Autowired
    AuthService authService;

    @Autowired
    SimcardRepository simcardRepository;

    @Autowired
    DetalizationRepository detalizationRepository;

    @Autowired
    MeasurementRepository measurementRepository;


    @Transactional
    public ApiResponse makeCAll(String callType, Integer minutes) {
        User loggedUser = authService.getLoggedUser();
        Simcard simcard = loggedUser.getSimcard();
        Integer callDuration=0;

        if(!simcard.isActive())
            return new ApiResponse("sim karta aktiv emas",  false);

        //tarmoq ichidagi qo'ng'iroqlar
        if(callType.equals("in")) {
            while (minutes>0) {
                if(simcard.getTariffInNetworkMinute()>0)
                    simcard.setTariffInNetworkMinute(simcard.getTariffInNetworkMinute() - 1);
                else if(simcard.getPacketInNetworkMinute()>0)
                    simcard.setPacketInNetworkMinute(simcard.getPacketInNetworkMinute()-1);
                else if(simcard.getBalance()>=simcard.getTariff().getInNetworkCallPrice())
                    simcard.setBalance(simcard.getBalance()-simcard.getTariff().getInNetworkCallPrice());
                else if(simcard.isPostpaid()) //qarzga yozib borish
                    simcard.setBalance(simcard.getBalance()-simcard.getTariff().getInNetworkCallPrice());
                else if(callDuration==0)
                    return new ApiResponse("qo'ng'iroqni amalga oshirolmaysiz", false);
                else {
                    Measurement measurement = measurementRepository.findByName("daqiqa");
                    Detalization detalization=new Detalization();
                    detalization.setAmount(callDuration);
                    detalization.setUserAction(UserAction.IN_NETWORK_CALL);
                    detalization.setBy(loggedUser.getId());
                    detalization.setMeasurement(measurement);
                    detalizationRepository.save(detalization);
                    return new ApiResponse("qo'ng'iroq tugatildi: " + callDuration + " daqiqa", true);
                }

                simcardRepository.save(simcard);
                minutes--;
                callDuration++;
                try {
                    Thread.sleep(1 * 60);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            Measurement measurement = measurementRepository.findByName("daqiqa");
            Detalization detalization=new Detalization();
            detalization.setAmount(callDuration);
            detalization.setUserAction(UserAction.IN_NETWORK_CALL);
            detalization.setBy(loggedUser.getId());
            detalization.setMeasurement(measurement);
            detalizationRepository.save(detalization);
        }        //tarmoq ichidagi qo'ng'iroqlar
        else if(callType.equals("out")) {
            while (minutes>0) {
                if(simcard.getTariffOutNetworkMinute()>0)
                    simcard.setTariffOutNetworkMinute(simcard.getTariffOutNetworkMinute() - 1);
                else if(simcard.getPacketOutNetworkMinute()>0)
                    simcard.setPacketOutNetworkMinute(simcard.getPacketOutNetworkMinute()-1);
                else if(simcard.getBalance()>=simcard.getTariff().getOutNetworkCallPrice())
                    simcard.setBalance(simcard.getBalance()-simcard.getTariff().getOutNetworkCallPrice());
                else if(simcard.isPostpaid()) //qarzga yozib borish
                    simcard.setBalance(simcard.getBalance()-simcard.getTariff().getOutNetworkCallPrice());
                else if(callDuration==0)
                    return new ApiResponse("qo'ng'iroqni amalga oshirolmaysiz", false);
                else {
                    Measurement measurement = measurementRepository.findByName("daqiqa");
                    Detalization detalization=new Detalization();
                    detalization.setAmount(callDuration);
                    detalization.setUserAction(UserAction.OUT_NETWORK_CALL);
                    detalization.setBy(loggedUser.getId());
                    detalization.setMeasurement(measurement);
                    detalizationRepository.save(detalization);
                    return new ApiResponse("qo'ng'iroq tugatildi: " + callDuration + " daqiqa", true);
                }

                simcardRepository.save(simcard);
                minutes--;
                callDuration++;
                try {
                    Thread.sleep(1 * 60);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            Measurement measurement = measurementRepository.findByName("daqiqa");
            Detalization detalization=new Detalization();
            detalization.setAmount(callDuration);
            detalization.setUserAction(UserAction.OUT_NETWORK_CALL);
            detalization.setBy(loggedUser.getId());
            detalization.setMeasurement(measurement);
            detalizationRepository.save(detalization);
        }


        return new ApiResponse("qo'ng'iroq tugatildi: "+callDuration+" dAqiqa", true);
    }


    @Transactional
    public ApiResponse sms() {
        User loggedUser = authService.getLoggedUser();
        Simcard simcard = loggedUser.getSimcard();

        if(simcard.getTariffSms()>0)
            simcard.setTariffSms(simcard.getTariffSms() - 1);
        else if(simcard.getPacketSms()>0)
            simcard.setPacketSms(simcard.getPacketSms()-1);
        else if(simcard.getBalance()>simcard.getTariff().getSmsPrice())
            simcard.setBalance(simcard.getBalance()-simcard.getTariff().getSmsPrice());
        else if(simcard.isPostpaid()) //qarzga yozib borish
            simcard.setBalance(simcard.getBalance()-simcard.getTariff().getSmsPrice());
        else
            return new ApiResponse("sms yuborolmaysiz", false);

        simcardRepository.save(simcard);

        Measurement measurement = measurementRepository.findByName("ta");
        Detalization detalization=new Detalization();
        detalization.setAmount(1);
        detalization.setUserAction(UserAction.SMS_SEND);
        detalization.setBy(loggedUser.getId());
        detalization.setMeasurement(measurement);
        detalizationRepository.save(detalization);

        return new ApiResponse("sms jo'natidi", true);
    }

    public ApiResponse internet( Integer megabytes) {
        User loggedUser = authService.getLoggedUser();
        Simcard simcard = loggedUser.getSimcard();
        Integer usedMb=0;

        while (megabytes>0) {
            if(simcard.getTariffMegabyte()>0)
                simcard.setTariffMegabyte(simcard.getTariffMegabyte() - 1);
            else if(simcard.getPacketMegabyte()>0)
                simcard.setPacketMegabyte(simcard.getPacketMegabyte()-1);
            else if(simcard.getBalance()>=simcard.getTariff().getMegabytePrice())
                simcard.setBalance(simcard.getBalance()-simcard.getTariff().getMegabytePrice());
            else if(simcard.isPostpaid()) //qarzga yozib borish
                simcard.setBalance(simcard.getBalance()-simcard.getTariff().getMegabytePrice());
            else if(usedMb.doubleValue()==0)
                return new ApiResponse("balansda mablag' yetmaydi", false);
            else {
                Measurement measurement = measurementRepository.findByName("mb");
                Detalization detalization=new Detalization();
                detalization.setAmount(usedMb);
                detalization.setUserAction(UserAction.TRAFFIC_USE);
                detalization.setBy(loggedUser.getId());
                detalization.setMeasurement(measurement);
                detalizationRepository.save(detalization);
                return new ApiResponse("interen: " + usedMb + " mb ishlatildi", true);
            }

            simcardRepository.save(simcard);
            megabytes--;
            usedMb++;
        }
        Measurement measurement = measurementRepository.findByName("mb");
        Detalization detalization=new Detalization();
        detalization.setAmount(usedMb);
        detalization.setUserAction(UserAction.TRAFFIC_USE);
        detalization.setBy(loggedUser.getId());
        detalization.setMeasurement(measurement);
        detalizationRepository.save(detalization);

        return new ApiResponse("interen: " + usedMb + " mb ishlatildi", true);
    }
}
