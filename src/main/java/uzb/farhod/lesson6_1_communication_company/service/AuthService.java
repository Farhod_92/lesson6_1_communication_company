package uzb.farhod.lesson6_1_communication_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson6_1_communication_company.entity.Filial;
import uzb.farhod.lesson6_1_communication_company.entity.Role;
import uzb.farhod.lesson6_1_communication_company.entity.User;
import uzb.farhod.lesson6_1_communication_company.entity.enums.RoleName;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.payload.ClientRegisterDto;
import uzb.farhod.lesson6_1_communication_company.payload.LoginDto;
import uzb.farhod.lesson6_1_communication_company.payload.StaffRegisterDto;
import uzb.farhod.lesson6_1_communication_company.repository.FilialRepository;
import uzb.farhod.lesson6_1_communication_company.repository.RoleRepository;
import uzb.farhod.lesson6_1_communication_company.repository.UserRepository;
import uzb.farhod.lesson6_1_communication_company.security.JwtProvider;

import java.util.Optional;

@Service
public class AuthService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    FilialRepository filialRepository;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    public ApiResponse registerManagers(StaffRegisterDto staffRegisterDto){
        boolean existsByUsername = userRepository.existsByUsername(staffRegisterDto.getUsername());
        if(existsByUsername)
            return new ApiResponse("bunaqa user bor", false);

        RoleName roleName = RoleName.getRoleNameByStringName(staffRegisterDto.getRoleName());
        Optional<Role> optionalRole = roleRepository.findByName(roleName);
        if(!optionalRole.isPresent())
            return new ApiResponse("rol topilmadi", false);
        Role newUserRole = optionalRole.get();

        Optional<Filial> optionalFilial = filialRepository.findById(staffRegisterDto.getFilialId());
        if(!optionalFilial.isPresent())
            return new ApiResponse("filial topilmadi", false);

        User user=new User();
        user.setFirstName(staffRegisterDto.getFirstName());
        user.setLastName(staffRegisterDto.getLastName());
        user.setLastName(staffRegisterDto.getLastName());
        user.setUsername(staffRegisterDto.getUsername());
        user.setPassword(passwordEncoder.encode(staffRegisterDto.getPassword()));
        user.setFilial(optionalFilial.get());
        user.setRole(newUserRole);
        userRepository.save(user);
        return new  ApiResponse("yangi user qo'shildi!", true);
    }

    public ApiResponse registerStaff(StaffRegisterDto staffRegisterDto){

        if(staffRegisterDto.getRoleName().equals(RoleName.ROLE_DIRECTOR.name()) ||
                staffRegisterDto.getRoleName().equals(RoleName.ROLE_FILIAL_MANAGER.name()) ||
                staffRegisterDto.getRoleName().equals(RoleName.ROLE_TARIFF_MANAGER.name()) )
            return new ApiResponse("bu roldagi userni faqat director qo'shishi mumkin", false);

        if (staffRegisterDto.getRoleName().equals(RoleName.ROLE_FILIAL_DIRECTOR.name()) &&
            !getLoggedUser().getRole().getName().equals(RoleName.ROLE_FILIAL_MANAGER))
            return new ApiResponse("bu roldagi userni faqat director yoki filial manageri qo'shishi mumkin", false);

       if(!getLoggedUser().getFilial().getId().equals(staffRegisterDto.getFilialId()))
           return new ApiResponse("siz faqat o'z filialingizga xodim qo'shingiz mumkin", false);

        boolean existsByUsername = userRepository.existsByUsername(staffRegisterDto.getUsername());
        if(existsByUsername)
            return new ApiResponse("bunaqa user bor", false);

        RoleName roleName = RoleName.ROLE_STAFF;
        Optional<Role> optionalRole = roleRepository.findByName(roleName);
        if(!optionalRole.isPresent())
                return new ApiResponse("rol topilmadi", false);
        Role newUserRole = optionalRole.get();

            User user=new User();
            user.setFirstName(staffRegisterDto.getFirstName());
            user.setLastName(staffRegisterDto.getLastName());
            user.setLastName(staffRegisterDto.getLastName());
            user.setUsername(staffRegisterDto.getUsername());
            user.setPassword(passwordEncoder.encode(staffRegisterDto.getPassword()));
            user.setFilial(getLoggedUser().getFilial());
            user.setRole(newUserRole);
            userRepository.save(user);
            return new  ApiResponse("yangi user qo'shildi!", true);
    }

    public ApiResponse registerClient(ClientRegisterDto clientRegisterDto){

        if(!clientRegisterDto.getRoleName().equals(RoleName.ROLE_CLIENT.name()))
            return new ApiResponse("bu yerda faqat client qo'shish mumkin", false);

        boolean existsByUsername = userRepository.existsByUsername(clientRegisterDto.getPhoneNumber());
        if(existsByUsername)
            return new ApiResponse("bu raqam registratsiyada o'tkan", false);

        RoleName roleName = RoleName.ROLE_CLIENT;
        Optional<Role> optionalRole = roleRepository.findByName(roleName);
        if(!optionalRole.isPresent())
            return new ApiResponse("rol topilmadi", false);
        Role newUserRole = optionalRole.get();

        User user=new User();
        user.setFirstName(clientRegisterDto.getFirstName());
        user.setLastName(clientRegisterDto.getLastName());
        user.setLastName(clientRegisterDto.getLastName());
        user.setUsername(clientRegisterDto.getPhoneNumber());
        user.setPassword(passwordEncoder.encode(clientRegisterDto.getPassword()));
        user.setFilial(getLoggedUser().getFilial());
        user.setRole(newUserRole);
        user.setJuridical(clientRegisterDto.isJuridical());
        userRepository.save(user);
        return new  ApiResponse(user, true);
    }


    public ApiResponse login(LoginDto loginDto){
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
        User user=(User) authentication.getPrincipal();
        String token = jwtProvider.generateToken(user.getUsername(), user.getRole());
        return new ApiResponse("Bearer "+token, true);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        return optionalUser.orElseThrow(()->new UsernameNotFoundException("user topilmadi"));
    }

    public User getLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null
                && authentication.isAuthenticated()
                && !authentication.getPrincipal().equals("anonymousUser")) {
            User loggedUser = (User) authentication.getPrincipal();
            return  loggedUser;
        }
        return null;
    }

}
