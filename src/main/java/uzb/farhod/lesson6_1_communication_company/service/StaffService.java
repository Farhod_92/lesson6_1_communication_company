package uzb.farhod.lesson6_1_communication_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson6_1_communication_company.entity.Filial;
import uzb.farhod.lesson6_1_communication_company.entity.User;
import uzb.farhod.lesson6_1_communication_company.entity.enums.RoleName;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.payload.StaffEditDto;
import uzb.farhod.lesson6_1_communication_company.payload.StaffRegisterDto;
import uzb.farhod.lesson6_1_communication_company.repository.FilialRepository;
import uzb.farhod.lesson6_1_communication_company.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class StaffService {
    @Autowired
    AuthService authService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    FilialRepository filialRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public List<User> getAll(){
        User loggedUser = authService.getLoggedUser();
        Filial filial = loggedUser.getFilial();
        return userRepository.findAllByFilial(filial);
    }

    public User getOne(UUID id){
        User loggedUser = authService.getLoggedUser();
        Filial loggedUserFilial = loggedUser.getFilial();
        User user = userRepository.getById(id);

        if(user.getFilial().equals(loggedUserFilial) || user.getId().equals(loggedUser.getId()))
            return user;
        else
            return null;
    }

    public ApiResponse edit(UUID id, StaffEditDto staffEditDto){
        User loggedUser = authService.getLoggedUser();

        if(!loggedUser.getFilial().getId().equals(staffEditDto.getFilialId()) && !loggedUser.getId().equals(id))
            return new ApiResponse("siz bu userni tahrirlay olmaysiz", false);

        Optional<User> op = userRepository.findById(id);
        if(!op.isPresent())
            return new ApiResponse("user topilmadi", false);

        Optional<Filial> optionalFilial = filialRepository.findById(staffEditDto.getFilialId());
        if(!optionalFilial.isPresent())
            return new ApiResponse("filial topilmadi", false);

        User user= op.get();
        user.setFirstName(staffEditDto.getFirstName());
        user.setLastName(staffEditDto.getLastName());
        user.setLastName(staffEditDto.getLastName());
        user.setUsername(staffEditDto.getUsername());
        user.setPassword(passwordEncoder.encode(staffEditDto.getPassword()));
        user.setFilial(optionalFilial.get());
        userRepository.save(user);
        return new  ApiResponse("user tahrirlandi!", true);
    }

    public ApiResponse delete(UUID id){
        User loggedUser = authService.getLoggedUser();
        User deletingUser = userRepository.getById(id);
        if(loggedUser.getFilial().equals(deletingUser.getFilial())) {
            userRepository.deleteById(id);
            return new ApiResponse("xodim o'chirildi", true);
        }else
            return new ApiResponse("siz bu xodimni o'chirolmaysiz", false);

    }

}
