package uzb.farhod.lesson6_1_communication_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson6_1_communication_company.payload.ApiResponse;
import uzb.farhod.lesson6_1_communication_company.entity.enums.UssdCode;

@Service
public class UssdService {
    @Autowired
    ClientService clientService;

    public ApiResponse executeUssd(String ussd){

        /**
         * 1#tarifId - tarifni almashtirish
         * masalan 1#12
          */
        if(ussd.startsWith("1")){
            String[] split = ussd.split("@");
            return clientService.changeTariff(Integer.parseInt(split[1]));
        }

            /**
         * 1#packetId - packetlarni xarid qilish
         * masalan 2@12
          */
        if(ussd.startsWith("2")){
            String[] split = ussd.split("@");
            return clientService.buyPacket(Integer.parseInt(split[1]));
        }

        if(ussd.startsWith("3")){
            return clientService.seeTariff();
        }

        return new ApiResponse("ussd komanda topilmadi", false);
    }
}
