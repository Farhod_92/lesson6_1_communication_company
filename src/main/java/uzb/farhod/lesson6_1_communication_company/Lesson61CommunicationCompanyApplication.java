package uzb.farhod.lesson6_1_communication_company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson61CommunicationCompanyApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson61CommunicationCompanyApplication.class, args);
        //https://docs.google.com/document/d/1oGjZhS57B4ANcmOrWHUK546RQ4SQOdEjz9OOVzbI69w/edit
    }

}
