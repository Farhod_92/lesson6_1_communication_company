package uzb.farhod.lesson6_1_communication_company.payload;

import lombok.Getter;
import uzb.farhod.lesson6_1_communication_company.entity.User;
import uzb.farhod.lesson6_1_communication_company.entity.enums.PaymentType;

import javax.persistence.ManyToOne;
import java.util.UUID;

@Getter
public class PaymentDto {

    private String paymentType;

    private double amount;

    private String toNumber;
}
