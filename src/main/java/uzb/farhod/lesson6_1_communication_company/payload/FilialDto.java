package uzb.farhod.lesson6_1_communication_company.payload;

import lombok.Getter;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
public class FilialDto {

    @NotNull
    private String name;

    private UUID directorId;
}
