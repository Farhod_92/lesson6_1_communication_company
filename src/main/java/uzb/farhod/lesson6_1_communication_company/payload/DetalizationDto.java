package uzb.farhod.lesson6_1_communication_company.payload;

import lombok.Getter;
import uzb.farhod.lesson6_1_communication_company.entity.Measurement;
import uzb.farhod.lesson6_1_communication_company.entity.enums.UserAction;

import javax.persistence.ManyToOne;

@Getter
public class DetalizationDto {
    private String userAction;
    private double amount;
    private Integer measurementId;
}
