package uzb.farhod.lesson6_1_communication_company.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ApiResponse {
    private String message;
    private boolean success;
    private Object object;

    public ApiResponse( Object object,boolean success) {
        this.object = object;
        this.success = success;
    }

    public ApiResponse(String message, boolean success) {
        this.message = message;
        this.success = success;
    }
}
