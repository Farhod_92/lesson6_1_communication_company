package uzb.farhod.lesson6_1_communication_company.payload;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
public class ClientRegisterDto {
    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    @Pattern(regexp = "^[+][9][9][8][9][3,4][0-9]{7}$")
    private String phoneNumber;

    @NotNull
    private String password;

    @NotNull
    private String roleName;

    @NotNull
    private boolean juridical;

    @NotNull
    private Integer tarifId;

    //boshlang'ich to'lov
    private double payment;
}
