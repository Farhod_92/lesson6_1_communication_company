package uzb.farhod.lesson6_1_communication_company.payload;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class LoginDto {
    @NotNull
    private String username;

    @NotNull
    private String password;
}
