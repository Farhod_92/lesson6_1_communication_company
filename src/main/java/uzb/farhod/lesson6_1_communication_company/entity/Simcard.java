package uzb.farhod.lesson6_1_communication_company.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Simcard {
    @Id
    @GeneratedValue
    private UUID id;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(optional = false,fetch = FetchType.LAZY)
    private User user;

    private boolean active;

    private boolean postpaid;

    private double balance=0;

    @ManyToOne(fetch = FetchType.LAZY)
    private Tariff tariff;
    private LocalDate tariffActivationDate;
    private double tariffMegabyte;
    private double tariffInNetworkMinute;
    private double tariffOutNetworkMinute;
    private double tariffSms;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Packet> packets;
    private double packetMegabyte;
    private double packetInNetworkMinute;
    private double packetOutNetworkMinute;
    private double packetSms;
    private LocalDate packetExpireDate;

    @OneToMany
    private List<EntertaimentService> entertaimentServices;
}
