package uzb.farhod.lesson6_1_communication_company.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import uzb.farhod.lesson6_1_communication_company.entity.enums.PaymentType;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Payment {
    @Id
    @GeneratedValue
    private UUID id;

    @CreationTimestamp
    private Timestamp time;

    @CreatedBy
    private UUID payedBy;

    @Enumerated(EnumType.STRING)
    private PaymentType paymentType;

    private double amount;

    @ManyToOne
    private User toNumber;
}
