package uzb.farhod.lesson6_1_communication_company.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uzb.farhod.lesson6_1_communication_company.entity.enums.UserAction;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Packet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private UserAction packetType;

    private String name;

    private double price;

    private Integer expireAfterDays;

    private boolean remainder;

    private boolean inNetwork;

    private double amount;

    @ManyToOne
    private Measurement measurement;

    @ManyToMany
    private List<Tariff> tariffs;

}
