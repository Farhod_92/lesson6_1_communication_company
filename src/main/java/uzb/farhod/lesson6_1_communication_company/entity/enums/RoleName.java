package uzb.farhod.lesson6_1_communication_company.entity.enums;

import lombok.Getter;

@Getter
public enum RoleName {
    ROLE_DIRECTOR,
    ROLE_FILIAL_MANAGER,
    ROLE_FILIAL_DIRECTOR,
    ROLE_NUMBER_MANAGER,
    ROLE_STAFF_MANAGER,
    ROLE_TARIFF_MANAGER,
    ROLE_STAFF,
    ROLE_CLIENT;

    public static RoleName getRoleNameByStringName(String name){
        switch (name){
            case "ROLE_DIRECTOR":
                return ROLE_DIRECTOR;
            case "ROLE_FILIAL_MANAGER":
                return ROLE_FILIAL_MANAGER;
            case "ROLE_FILIAL_DIRECTOR":
                return ROLE_FILIAL_DIRECTOR;
            case "ROLE_NUMBER_MANAGER":
                return ROLE_NUMBER_MANAGER;
            case "ROLE_STAFF_MANAGER":
                return ROLE_STAFF_MANAGER;
            case "ROLE_TARIFF_MANAGER":
                return ROLE_TARIFF_MANAGER;
            case "ROLE_STAFF":
                return ROLE_STAFF;
            case "ROLE_CLIENT":
                return ROLE_CLIENT;
            default:
                return null;
        }
    }
}
