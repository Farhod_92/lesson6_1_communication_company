package uzb.farhod.lesson6_1_communication_company.entity.enums;

public enum PaymentType {
    NAQD,
    PUL_KOCHIRISH,
    PLASTIK,
}
