package uzb.farhod.lesson6_1_communication_company.entity.enums;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum UserAction {
    IN_NETWORK_CALL,
    OUT_NETWORK_CALL,
    SMS_SEND,
    TRAFFIC_USE,
    CHANGE_TARIFF,
    BUY_PACKET
}
