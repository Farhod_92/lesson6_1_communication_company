package uzb.farhod.lesson6_1_communication_company.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Getter
@AllArgsConstructor
public enum UssdCode {
    MAIN("*111#"),
    NEXT("##"),
    CHANGE_TARIFF("*120#");

    private final String command;
}
