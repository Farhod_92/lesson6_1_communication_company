package uzb.farhod.lesson6_1_communication_company.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import uzb.farhod.lesson6_1_communication_company.entity.enums.RoleName;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Tariff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    //true=yuridik shaxs uchun false=jismoniy shaxs uchun
    private boolean juridical;

    private double price;
    private double priceToSwitch;
    private Date expireDate;

    private double megabyte;
    private Integer inNetworkMinute;
    private Integer outNetworkMinute;
    private Integer sms;

    private Float megabytePrice;
    private Float inNetworkCallPrice;
    private Float outNetworkCallPrice;
    private Float smsPrice;

    private LocalDate tariffActivationDate=LocalDate.now();
}
