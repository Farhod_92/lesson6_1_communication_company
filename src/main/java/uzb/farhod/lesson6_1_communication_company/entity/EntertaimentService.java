package uzb.farhod.lesson6_1_communication_company.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uzb.farhod.lesson6_1_communication_company.entity.enums.UserAction;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class EntertaimentService {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Enumerated(EnumType.STRING)
    private UserAction serviceType;

    private double priceForDay;

    private double priceForMonth;

}
