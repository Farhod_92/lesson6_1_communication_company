package uzb.farhod.lesson6_1_communication_company.security;

import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Jwts;
import uzb.farhod.lesson6_1_communication_company.entity.Role;
import uzb.farhod.lesson6_1_communication_company.repository.UserRepository;

import java.util.Date;
import java.util.Set;

@Component
public class JwtProvider {

    private String secretKey="my super secret key is very long";
    private long expireTime=1000*60*60*24;

    public String generateToken(String username, Role role){
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + expireTime))
                .claim("role", role)
                .compact();
        return token;
    }

    public String getUsernameFromToken(String token){
        String username = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return username;
    }

}
